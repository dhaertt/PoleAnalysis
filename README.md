# EHMate

EHMate is a Python package to analyze time-series of pole deflection experiments with engineered human myocardium (EHM). 

EHMate automatically ..
1. determines the baseline pole position, 
2. detects contraction peaks with an adaptive noise-dependent 
threshold, 
3. calculates beating frequencies and arrhythmia features based on peak-to-peak time intervals, 
4. robustly analyzes various peak shape features (amplitude, contraction time, ...) by fitting a skew Gaussian distribution and 
5. stores all results and used parameters in a pandas dataframe.

A detailed description of the package structure, functions and 
parameters is found in the function docstrings and a [tutorial Jupyter notebook](dev.ipynb).

For questions and inquiries, contact the author [Daniel Härtter](mailto:daniel.haertter@med.uni-goettingen.de).

EHMate is licensed under the [GNU General Public License v3.0](LICENSE).


