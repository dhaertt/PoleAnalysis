import glob
import os

import pandas as pd
from tqdm import tqdm as tqdm

from .measurement import Measurement


class Experiment:
    def __init__(self, folder, user=None, plate='myrplate48'):
        """
        Initialize experiment object

        Parameters
        ----------
        folder : str
            Folder with .csv files of measurements of one plate
        user : str
            User name
        plate : str
            Plate model (e.g. 'myrplate48')
        """
        self.folder = folder
        self.experiment = os.path.basename(folder[:-1])
        self.user = user
        self.plate = plate
        # find all csv files of measurements
        self.csv_files = glob.glob(self.folder + '*.csv')
        if len(self.csv_files) == 0:
            raise FileNotFoundError('No csv files found in folder.')
        # check whether dataframe exists and load or create dataframe for all measurements
        if os.path.exists(folder + f'df_{self.experiment}.pk'):
            self.df_experiment = pd.read_pickle(folder + f'df_{self.experiment}.pk')
            print('Dataframe from previous analysis found and loaded.')
        else:
            self.df_experiment = pd.DataFrame()

    def analyze_measurements(self, filter_size=None, noise_threshold=2.5, peak_area_min=0.1, min_dist=0.5,
                             min_freq=0.25, width_area=5, fit_skew_gaussian=True, width_fit=15, filter_params=(9, 4),
                             restart=False):
        """
        Iterate through all .csv files and analyze all traces. Results are all stored in one DataFrame.

        Parameters
        ----------
        filter_size : int
            Filter size for rolling median for baseline estimation (for no filtering, set filter_size=None)
        noise_threshold : float
            Factor for noise threshold for peak detection. The absolute threshold is noise_threshold*noise_std with
            the noise amplitude noise_std.
        peak_area_min : float
            Minimal area under curve of peaks. Peaks with area smaller than peak_area_min are removed.
        min_dist : float
            Minimal distance between peaks in seconds.
        min_freq : float
            Minimal beating frequency. If frequency smaller than min_freq, no peaks are detected.
        width_area : int
            Interval left and right of peak maximum (in time points) in which area under curve is calculated.
        fit_skew_gaussian : bool
            If True, skew-Gaussian is fit to every detected peak to determine T-10 and min/max velocities. If False,
            no skew Gaussian is fitted (for arrhythmias).
        width_fit : int
            Crop interval left and right of peak maximum (in time points) for fitting of skew Gaussian.
        filter_params : tuple(int, int)
            (window_length, polyorder) of Savitzky-Golay filter for smoothing trace prior to fitting skew Gaussian
        restart : bool
            If True, all csv files are analyzed (if e.g. parameters changed). If False, already analyzed csv files are
            skipped.
        """
        if 'csv_file' in self.df_experiment.keys():
            csv_files_analyzed = self.df_experiment['csv_file'].unique()
        else:
            csv_files_analyzed = []
        for i, csv_file in enumerate(tqdm(self.csv_files)):
            if csv_file not in csv_files_analyzed or restart:
                measurement_i = Measurement(csv_file, user=self.user, plate=self.plate)
                measurement_i.analyze_traces(filter_size, noise_threshold, peak_area_min, min_dist, min_freq,
                                             width_area, fit_skew_gaussian, width_fit, filter_params, reduce=True)
                self.df_experiment = pd.concat([self.df_experiment, measurement_i.df_traces], ignore_index=True)
        self.store_df_experiment()

    def store_df_experiment(self):
        experiment_names = self.df_experiment['experiment'].unique()
        if len(experiment_names) > 1:
            raise ValueError('Folder contains csv files of more than one experiment.')
        else:
            experiment_name = experiment_names[0]
        if not experiment_name == self.experiment:
            raise ValueError('Folder name does not match experiment name.')
        self.df_experiment.to_pickle(self.folder + f'/df_{experiment_name}.pk')
