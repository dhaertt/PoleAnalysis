# plot functions
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

# plot params
fontsize = 8
markersize = 3
labelpad = 0.
dpi = 300
save_format = 'png'

width_1cols = 3.5
width_1p5cols = 5
width_2cols = 7.1

plt.rcParams.update({'font.size': fontsize, 'axes.labelpad': labelpad})
plt.style.use('seaborn-paper')


def label_all_panels(axs, offset=(-0.1, 1.1)):
    for key in axs.keys():
        label_panel(axs[key], key, offset=offset)


def label_panel(ax, label, offset=(-0.1, 1.1)):
    ax.text(offset[0], offset[1], label, transform=ax.transAxes,
            fontsize=fontsize + 1, fontweight='bold', va='top', ha='right')


def remove_all_spines(axs):
    for key in axs.keys():
        remove_spines(axs[key])


def remove_spines(ax):
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)


def plot_trace_summary(trace_obj, peak_idx=1, title=None, filename=None):
    mosaic = """
    AAAAA
    BCDEF
    """
    fig, axs = plt.subplot_mosaic(mosaic, figsize=(8.5, 3.5))

    plot_trace(trace_obj, axs['A'])

    if len(trace_obj.trace_dict['peak_tps']) > 0:

        plot_hist_delta_tps(trace_obj, axs['B'])

        if len(trace_obj.trace_dict['peak_tps']) > peak_idx:
            plot_peak(trace_obj, axs['C'], i=peak_idx)
        else:
            raise ValueError('peak_idx too large.')

        plot_hist_amp_perc(trace_obj, axs['D'])

        keys = ['time_contr_10', 'time_relax_10']
        labels = ['T-C-10%', 'T-R-10%']
        plot_boxplot(trace_obj, axs['E'], keys, labels, 'Time [s]')

        keys = ['vel_contr', 'vel_relax']
        labels = ['Contr.', 'Relax.']
        plot_boxplot(trace_obj, axs['F'], keys, labels, ylabel='Velocity [mm/s]')

    fig.suptitle(title)
    plt.tight_layout(pad=1)

    if filename is not None:
        fig.savefig(filename, dpi=dpi)
    plt.show()


def plot_trace(trace_obj, ax):
    ax.plot(trace_obj.trace_dict['time'], trace_obj.trace_dict['trace_norm'], c='k')

    for i in trace_obj.trace_dict['peak_idxs']:
        plot_peak_ = ax.axvline(trace_obj.trace_dict['time'][i], alpha=0.5, c='r')
    if len(trace_obj.trace_dict['peak_idxs']) == 0:
        plot_peak_ = ax.axvline(-1, alpha=0.5, c='r')

    plot_noise_threshold = ax.axhline(
        trace_obj.trace_dict['noise_std'] * trace_obj.trace_dict['params.noise_threshold'],
        linestyle='--')

    ax.set_xlim(0, trace_obj.trace_dict['time'].max())
    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Contr. [mm]')
    ax.xaxis.set_major_locator(MultipleLocator(5))
    ax.xaxis.set_major_formatter(FormatStrFormatter('%g'))
    ax.xaxis.set_minor_locator(MultipleLocator(1))

    ax.legend([plot_noise_threshold, plot_peak_], ['Noise threshold', 'Peaks'], loc=1)


def plot_boxplot(trace_obj, ax, keys, labels, ylabel, xlabel=None, rotate=45):
    data = [trace_obj.trace_dict[key][~np.isnan(trace_obj.trace_dict[key])] for key in keys]
    ax.boxplot(data, whis=95, labels=labels, showfliers=False)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_xticklabels(labels, rotation=rotate)


def plot_hist_amp_perc(trace_obj, ax):
    key = 'peak_amp_perc'
    data = trace_obj.trace_dict[key][~np.isnan(trace_obj.trace_dict[key])] * 100
    ax.hist(data, rwidth=0.8, range=(0, 12), bins=20, color='gray')
    ax.set_ylabel('Count')
    ax.set_xlabel('Contraction amp. [%]')
    ax.xaxis.set_major_locator(MultipleLocator(2))
    ax.xaxis.set_major_formatter(FormatStrFormatter('%g'))
    ax.xaxis.set_minor_locator(MultipleLocator(0.5))


def plot_peak(trace_obj, ax, i=2):
    tp_i = trace_obj.trace_dict['peak_tps'][i]
    ax.plot(trace_obj.trace_dict['time'], trace_obj.trace_dict['trace_norm'], c='k')
    ax.axvline(tp_i, c='r')
    ax.set_xlim(tp_i - 0.5, tp_i + 0.5)
    ax.set_title(f'Peak # {i}')
    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Contr. [mm]')


def plot_hist_delta_tps(trace_obj, ax):
    delta = np.diff(trace_obj.trace_dict['peak_tps'])
    mean, std = trace_obj.trace_dict['delta_tps_mean'], trace_obj.trace_dict['delta_tps_std']
    skew, kurt = trace_obj.trace_dict['delta_tps_skew'], trace_obj.trace_dict['delta_tps_kurt']
    ax.hist(delta, range=(0, 6), bins=20, rwidth=0.8, color='gray')
    ax.set_xlabel('Time btw. peaks [s]')
    ax.set_ylabel('Count')
    text = f'mean={np.round(mean, 2)}\nstd={np.round(std, 2)}\nskew={np.round(skew, 2)}\nkurt.={np.round(kurt, 2)}'
    ax.legend(labels=[text], fontsize=fontsize-2, loc=1)
    ax.xaxis.set_major_locator(MultipleLocator(2))
    ax.xaxis.set_major_formatter(FormatStrFormatter('%g'))
    ax.xaxis.set_minor_locator(MultipleLocator(0.5))


def plot_measurement_summary(mea_obj, ylim=(-0.05, 0.15), filename=None):
    n_rows = len(mea_obj.df_traces)
    fig, axs = plt.subplots(figsize=(width_2cols, n_rows * 0.7), nrows=n_rows)

    for i, trace_i in enumerate(mea_obj.df_traces.iterrows()):
        trace_norm_i = trace_i[1]['trace_norm']
        time_i = trace_i[1]['time']
        axs[i].plot(time_i, trace_norm_i, lw=0.5, c='k')
        axs[i].set_xticklabels([])
        axs[i].set_ylim(ylim)
        axs[i].set_xlim(0, time_i.max())
        peak_tps_i = trace_i[1]['peak_tps']
        for tp in peak_tps_i:
            axs[i].axvline(tp, lw=1, c='r', alpha=0.6)
        axs[i].text(-5, 0, str(i))

    axs[-1].set_xlabel('Time [s]')
    plate_name = mea_obj.meta_dict['experiment']
    date = mea_obj.meta_dict['timestamp'].strftime('%Y%m%d')
    age = mea_obj.meta_dict['age']
    title = f'Plate: {plate_name}, Date: {date}, Age: {age} days'
    fig.suptitle(title)
    plt.tight_layout()

    if filename is not None:
        fig.savefig(filename, dpi=dpi)
