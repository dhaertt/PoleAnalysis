import datetime
import re

import numpy as np
import pandas as pd

from .plots import plot_trace_summary
from .trace_analysis import TraceAnalysis
from .helper import idx_to_well_name, reduce_dataframe


class Measurement:
    def __init__(self, csv_file, user=None, plate='myrplate48'):
        """
        Initialize measurement object

        Parameters
        ----------
        csv_file : str
            .csv file containing traces
        user : str
            User name
        plate : str
            Plate model (e.g. 'myrplate48')
        """
        self.csv_file = csv_file
        self.xml_meta = csv_file[:-4] + '.xml'

        # read .csv file with traces
        data = np.loadtxt(csv_file, delimiter=',')
        self.time, self.traces_px = data[:, 0], data[:, 1:].T
        self.framerate = 1 / np.mean(np.diff(self.time))

        # get metadata - from corrupt xml files by old
        with open(self.xml_meta) as fd:
            meta_str = fd.read()
        date_str = re.findall('Date>(\d{4})(\d{2})(\d{2})', meta_str)[0]
        time_str = re.findall('Time>(\d{2}):(\d{2}):(\d{2})<', meta_str)[0]
        timestamp = datetime.datetime(int(date_str[0]), int(date_str[1]), int(date_str[2]),
                                      int(time_str[0]), int(time_str[1]), int(time_str[2]))
        weekday = timestamp.weekday()
        experiment = re.findall('ExperimentName>(\w+)<', meta_str)[0]
        casting_date = datetime.datetime.strptime(experiment[:8], '%Y%m%d')
        age = (timestamp - casting_date).days
        resolution = float(re.findall('pixel_per_mm>(\d+\.\d+)<', meta_str)[0])
        self.meta_dict = {'timestamp': timestamp, 'casting_date': casting_date, 'weekday': weekday, 'age': age,
                          'experiment': experiment, 'resolution': resolution, 'user': user, 'plate': plate,
                          'csv_file': csv_file}
        self.traces = self.traces_px / resolution
        self.df_traces = None

    def analyze_traces(self, filter_size=None, noise_threshold=2.5, peak_area_min=0.1, min_dist=0.5, min_freq=0.25,
                       width_area=5, fit_skew_gaussian=True, width_fit=15, filter_params=(9, 4), reduce=False,
                       plot=False):
        """
        Analyze all traces of measurement and create dictionary with results (keys see docstring

        Parameters
        ----------
        filter_size : int
            Filter size for rolling median for baseline estimation (for no filtering, set filter_size=None)
        noise_threshold : float
            Factor for noise threshold for peak detection. The absolute threshold is noise_threshold*noise_std with
            the noise amplitude noise_std.
        peak_area_min : float
            Minimal area under curve of peaks. Peaks with area smaller than peak_area_min are removed.
        min_dist : float
            Minimal distance between peaks in seconds.
        min_freq : float
            Minimal beating frequency. If frequency smaller than min_freq, no peaks are detected.
        width_area : int
            Interval left and right of peak maximum (in time points) in which area under curve is calculated.
        fit_skew_gaussian : bool
            If True, skew-Gaussian is fit to every detected peak to determine T-10 and min/max velocities. If False,
            no skew Gaussian is fitted (for arrhythmias).
        width_fit : int
            Crop interval left and right of peak maximum (in time points) for fitting of skew Gaussian.
        filter_params : tuple(int, int)
            (window_length, polyorder) of Savitzky-Golay filter for smoothing trace prior to fitting skew Gaussian
        reduce : bool
            If True, columns with arrays / lists / time-series are removed prior to storing.
        plot : bool
            If True, control plots for each trace analysis are shown.
        """
        dict_all = []
        # iterate traces / wells
        for i, trace in enumerate(self.traces):
            trace_obj = TraceAnalysis(trace, self.time, filter_size=filter_size, noise_threshold=noise_threshold,
                                      peak_area_min=peak_area_min, min_dist=min_dist, min_freq=min_freq,
                                      width_area=width_area, fit_skew_gaussian=fit_skew_gaussian, width_fit=width_fit,
                                      filter_params=filter_params, plot=False)
            # add metadata to dict
            trace_obj.trace_dict.update(self.meta_dict)
            trace_obj.trace_dict['well'] = idx_to_well_name(i)
            # append to list
            dict_all.append(trace_obj.trace_dict)
            if plot:
                plot_trace_summary(trace_obj, peak_idx=1, title=f'Well {idx_to_well_name(i)}')
        # convert to pandas dataframe
        self.df_traces = pd.DataFrame(dict_all)
        if reduce:
            self.df_traces = reduce_dataframe(self.df_traces)

    def store_df_traces(self, filename, reduce=True):
        """
        Store pandas dataframe with measurement results

        Parameters
        ----------
        filename : str
            Filename for storing pandas dataframe.
        reduce : bool
            If True, columns with arrays / lists / time-series are removed prior to storing.
        """
        if reduce:
            df_traces = reduce_dataframe(self.df_traces)
        else:
            df_traces = self.df_traces
        pd.to_pickle(df_traces, filename)
