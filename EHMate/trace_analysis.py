import numpy as np
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit
import peakutils
from matplotlib.ticker import FormatStrFormatter, MultipleLocator
from scipy import stats
from scipy.signal import savgol_filter
from scipy.stats import skewnorm
from scipy.ndimage import generic_filter


class TraceAnalysis:
    def __init__(self, trace, time, filter_size=None, noise_threshold=2.5, peak_area_min=0.1, min_dist=0.35,
                 min_freq=0.25, width_area=5, fit_skew_gaussian=True, width_fit=15, filter_params=(9, 4), plot=False):
        """
        Class for analysis of single trace. Upon initialization, traces are analyzed and a dictionary trace_dict with
        the results is created.

        1. Detection of baseline and noise amplitude
        2. Peak detection (parameters see below)
        3. Analysis of peak shapes (parameters see below)

        Parameters
        ----------
        trace : ndarray
            Time-series of pole distances.
        time : ndarray
            Time-series of time points.
        filter_size : int
            Filter size for rolling median for baseline estimation (for no filtering, set filter_size=None)
        noise_threshold : float
            Factor for noise threshold for peak detection. The absolute threshold is noise_threshold*noise_std with
            the noise amplitude noise_std.
        peak_area_min : float
            Minimal area under curve of peaks. Peaks with area smaller than peak_area_min are removed.
        min_dist : float
            Minimal distance between peaks in seconds.
        min_freq : float
            Minimal beating frequency. If frequency smaller than min_freq, no peaks are detected.
        width_area : int
            Interval left and right of peak maximum (in time points) in which area under curve is calculated.
        fit_skew_gaussian : bool
            If True, skew-Gaussian is fit to every detected peak to determine T-10 and min/max velocities. If False,
            no skew Gaussian is fitted (for arrhythmias).
        width_fit : int
            Crop interval left and right of peak maximum (in time points) for fitting of skew Gaussian.
        filter_params : tuple(int, int)
            (window_length, polyorder) of Savitzky-Golay filter for smoothing trace prior to fitting skew Gaussian
        plot : bool
            If True, control plots for each trace analysis are shown.
        """

        self.trace = trace
        self.time = time

        # baseline and noise analysis
        self.baseline, self.noise_std, self.trace_perc, self.trace_norm = get_baseline_noise(trace, plot,
                                                                                             filter_size=filter_size)

        # peak detection
        peak_idxs, peak_tps_0, frequency = detect_peaks(self.trace_norm, self.time, self.noise_std,
                                                        noise_threshold=noise_threshold,
                                                        peak_area_min=peak_area_min, min_dist=min_dist,
                                                        width_area=width_area, min_freq=min_freq, plot=plot)

        # fit skew normal distribution to peaks
        if fit_skew_gaussian:
            peak_dict = fit_peaks_skew_gaussian(self.time, self.trace_norm, self.trace_perc, idxs=peak_idxs,
                                                width_fit=width_fit, filter_params=filter_params, plot=plot)
        else:
            peak_dict = get_empty_peak_dict()
            peak_dict['peak_tps'] = peak_tps_0

        # analyze time between peaks delta
        delta_tps_mean, delta_tps_std, delta_tps_skew, delta_tps_kurt = analyze_delta_tps(peak_dict['peak_tps'],
                                                                                          plot=plot)

        # create dictionary
        self.trace_dict = {'time': time, 'trace': trace, 'trace_perc': self.trace_perc, 'trace_norm': self.trace_norm,
                           'baseline': self.baseline, 'noise_std': self.noise_std, 'peak_idxs': peak_idxs,
                           'frequency': frequency, 'delta_tps_mean': delta_tps_mean, 'delta_tps_std': delta_tps_std,
                           'delta_tps_skew': delta_tps_skew, 'delta_tps_kurt': delta_tps_kurt,
                           'params.noise_threshold': noise_threshold, 'params.min_dist': min_dist,
                           'params.peak_area_min': peak_area_min, 'params.min_freq': min_freq,
                           'params.width_area': width_area, 'params.fit_skew_gaussian': fit_skew_gaussian,
                           'params.width_fit': width_fit, 'params.filter_params': filter_params,
                           'params.filter_size': filter_size}
        self.trace_dict.update(peak_dict)


def get_baseline_noise(trace, plot=False, filter_size=None):
    """ Determine baseline (static) and amplitude of noise """
    # calculate baseline
    if filter_size is not None:
        baseline = generic_filter(trace, np.nanmedian, filter_size)
    else:
        baseline = np.nanmedian(trace)
    # calculate histogram
    counts, bins = np.histogram(trace-baseline, range=(-3, 3), bins=600)

    # determine noise level (width of fluctuation histogram around baseline)
    def gauss(x, a, sigma):
        return a * np.exp(-x ** 2 / (2 * sigma ** 2))

    p, pcov = curve_fit(gauss, bins[:-1], counts, p0=[3e3, 5e-1], maxfev=1000)
    noise_std = abs(p[1])
    trace_perc = - (trace / baseline - 1)
    trace_norm = -trace + baseline

    if plot:
        fig, ax = plt.subplots(figsize=(4, 2))
        ax.plot(bins[1:], counts, c='k', label='Data histogram')
        fit = gauss(bins, p[0], p[1])
        ax.plot(bins, fit, linestyle='--', c='r', label='Gaussian fit')
        ax.axvline(- noise_std, c='b', linestyle=':', label='Noise thres.')
        ax.set_ylabel('Counts')
        ax.set_xlabel('Pole deflection [mm]')
        ax.set_xlim(baseline - 0.2, baseline + 0.4)
        ax.legend()
        plt.show()

    return baseline, noise_std, trace_perc, trace_norm


def detect_peaks(trace_norm, time, noise_std, noise_threshold=2.5, min_dist=0.4, width_area=5, peak_area_min=0.1,
                 min_freq=0.25, plot=False):
    """ Detect peaks (parameters explained below)

    trace_norm : ndarray
        Normalized time-series of pole distances with resting length 0
    time : ndarray
        Time-series of time points.
    noise_threshold : float
        Factor for noise threshold for peak detection. The absolute threshold is noise_threshold*noise_std with
        the noise amplitude noise_std.
    min_dist : float
        Minimal distance between peaks in seconds.
    width_area : int
        Interval left and right of peak maximum (in time points) in which area under curve is calculated.
    peak_area_min : float
        Minimal area under curve of peaks. Peaks with area smaller than peak_area_min are removed.
    min_freq : float
        Minimal beating frequency. If frequency smaller than min_freq, no peaks are detected.
    plot : bool
        If True, control plot is shown for parameter optimization.

    """
    framerate = 1 / np.mean(np.diff(time))

    # peakfinder
    if np.count_nonzero(trace_norm) > 0:
        idxs = peakutils.indexes(trace_norm, thres=noise_threshold * noise_std,
                                 min_dist=int(min_dist * framerate), thres_abs=True)
        # remove peak if area under curve around peak (defined by width) is smaller than peak_area_min
        for n, idx in enumerate(idxs):
            trace_norm_window_n = trace_norm[idx - width_area:idx + width_area]
            peak_area_n = np.sum(trace_norm_window_n)
            if peak_area_n < peak_area_min:
                idxs[n] = 0
        idxs = idxs[idxs > 0]
        frequency = len(idxs) / len(trace_norm) * framerate
        if frequency < min_freq or len(idxs) == 0:
            idxs = []
            frequency = np.nan
        tps = time[idxs]
    else:
        idxs = []
        frequency = np.nan
        tps = np.asarray([])

    if plot:
        fig, ax = plt.subplots(figsize=(14, 2))
        ax.plot(time, trace_norm, c='k')
        for i in idxs:
            plot_peak = ax.axvline(time[i], alpha=0.5, c='r')
        if len(idxs) == 0:
            plot_peak = ax.axvline(-1, alpha=0.5, c='r')
        plot_noise_threshold = ax.axhline(noise_std * noise_threshold, linestyle='--')
        ax.set_xlim(0, time.max())
        ax.set_xlabel('Time [s]')
        ax.set_ylabel('Pole deflection [mm]')
        ax.xaxis.set_major_locator(MultipleLocator(5))
        ax.xaxis.set_major_formatter(FormatStrFormatter('%g'))
        ax.xaxis.set_minor_locator(MultipleLocator(1))
        ax.legend([plot_noise_threshold, plot_peak], ['Noise threshold', 'Peaks'])
    return idxs, tps, frequency


def skew_gaussian(time, ampl, center, width, skewness, offset):
    """Computes the skewed Gaussian function.

    Parameters
    ----------
    time : list
        Point to evaluate the Gaussian for.
    ampl : float
        Amplitude
    center : float
        Center of distribution
    width : float
        Width of distribution
    skewness : float
        Skewness of distribution
    offset : float
        Zero offset


    Returns
    -------
    float
        Value of the specified skewed Gaussian at *time*
    """
    return ampl * skewnorm.pdf(time, skewness, loc=center, scale=width) + offset


def fit_peaks_skew_gaussian(time, trace_norm, trace_perc, idxs, width_fit=15, filter_params=(9, 4), min_time=0.02,
                            max_vel=1, plot=False):
    """
    Analyze peak shapes by fitting skew Gaussian to single peaks.

    Parameters
    ----------
    time : ndarray
        Time-series of time points.
    trace_norm : ndarray
        Normalized time-series of pole distances with resting length 0.
    trace_perc : ndarray
        Normalized time-series of pole deflection in %.
    idxs : ndarray
        List of peak indices
    width_fit : int
        Crop interval left and right of peak maximum (in time points) for fitting of skew Gaussian.
    filter_params : tuple(int, int)
        (window_length, polyorder) of Savitzky-Golay filter for smoothing trace prior to fitting skew Gaussian
    min_time : float
        Minimal time for contraction / relaxation (time-to-peak). Values smaller are set to NaN
    max_vel : float
        Maximal contraction / relaxation velocity. Larger values are set to NaN
    plot : book
        If True, fitting results of single peaks are shown.

    Returns
    -------
    peak_dict : dict
        Dictionary with peak analysis results.
        List of keys:
        - 'peak_tps': Updated peak timepoints, time of maximum of fitted skew Gaussian.
        - 'peak_amp', 'peak_amp_mean', 'peak_amp_std': Peak amplitude in mm (list, mean, std)
        - 'peak_amp_perc', 'peak_amp_perc_mean', 'peak_amp_perc_mean': Peak amp in % (list, mean, std)
        - 'vel_contr', 'vel_contr_mean', 'vel_contr_std': Maximal absolute contraction velocity (list, mean, std)
        - 'vel_relax', 'vel_relax_mean', 'vel_relax_std': Maximal absolute relaxation velocity (list, mean, std)
        - 'time_contr_10', 'time_contr_10_mean', 'time_contr_10_std': Time from 10% to maximum contraction (list, mean, std)
        - 'time_relax_10', 'time_relax_10_mean', 'time_relax_10_std': Time from maximum to 10% relaxation (list, mean, std)
        - 'time_contr_50', 'time_contr_50_mean', 'time_contr_50_std': Time from 50% to maximum contraction (list, mean, std)
        - 'time_relax_50', 'time_relax_50_mean', 'time_relax_50_std': Time from maximum to 50% relaxation (list, mean, std)
        - 'params.min_time': min_time
        - 'params.max_vel': max_vel
    """
    # savitzky-golay smoothing of trace
    trace_filt = savgol_filter(trace_norm, filter_params[0], filter_params[1])

    # define output arrays
    peak_tps = time[idxs]
    peak_amp = np.zeros(len(idxs)) * np.nan
    peak_amp_perc = np.zeros(len(idxs)) * np.nan
    vel_contr = np.zeros(len(idxs)) * np.nan
    vel_relax = np.zeros(len(idxs)) * np.nan
    time_contr_10 = np.zeros(len(idxs)) * np.nan
    time_relax_10 = np.zeros(len(idxs)) * np.nan
    time_contr_50 = np.zeros(len(idxs)) * np.nan
    time_relax_50 = np.zeros(len(idxs)) * np.nan

    # iterate peaks and fit skew Gaussian
    for i, idx in enumerate(idxs):
        time_ = time[idx - width_fit: idx + width_fit]
        trace_ = trace_norm[idx - width_fit: idx + width_fit]
        trace_filt_ = trace_filt[idx - width_fit: idx + width_fit]
        if len(time_) > 1.5 * width_fit:
            try:
                initial = [1, time[idx], 0.5, 2, 0]
                p, pcov = curve_fit(skew_gaussian, time_, trace_filt_, p0=initial, maxfev=1000)
                time_fit = np.linspace(time[idx - width_fit], time[min(idx + width_fit, len(time) - 1)], 100)
                trace_fit = skew_gaussian(time_fit, *p)
                vel_fit = np.diff(trace_fit) / np.mean(np.diff(time))

                if plot:
                    fig, ax = plt.subplots(figsize=(4, 2))
                    ax.plot(time_, trace_, label='Raw', c='k', lw=0.5)
                    ax.plot(time_, trace_filt_, label='Smoothed', c='b', lw=1)
                    ax.plot(time_fit, trace_fit, label='Fit', c='r', lw=1)
                    ax.plot(time_fit[:-1], vel_fit, label='Velocity', c='g', lw=1)
                    ax.set_xlabel('Time [s]')
                    ax.set_ylabel('Pole deflection [mm]')
                    ax.legend()
                    plt.show()
                if p[0] > 0:  # if amplitude larger 0
                    peak_tps[i] = time_fit[np.argmax(trace_fit)]
                    peak_amp[i] = np.max(trace_)
                    peak_amp_perc[i] = trace_perc[idx]
                    vel_contr[i] = np.max(vel_fit)
                    vel_relax[i] = - np.min(vel_fit)
                    idx_fit = np.argmax(trace_fit)
                    dt_fit = np.diff(time_fit)[0]
                    y_fit_contr = trace_fit[:idx_fit + 1]
                    time_contr_10[i] = (idx_fit - np.argmin(np.abs(y_fit_contr - 0.1 * np.max(trace_fit)))) * dt_fit
                    time_contr_50[i] = (idx_fit - np.argmin(np.abs(y_fit_contr - 0.5 * np.max(trace_fit)))) * dt_fit
                    y_fit_relax = trace_fit[idx_fit:]
                    time_relax_10[i] = (np.argmin(np.abs(y_fit_relax - 0.1 * np.max(trace_fit)))) * dt_fit
                    time_relax_50[i] = (np.argmin(np.abs(y_fit_relax - 0.5 * np.max(trace_fit)))) * dt_fit
            except:
                pass

    # set to nan where 0
    time_contr_10[time_contr_10 < min_time] = np.nan
    time_contr_50[time_contr_50 < min_time] = np.nan
    time_relax_10[time_relax_10 < min_time] = np.nan
    time_relax_50[time_relax_50 < min_time] = np.nan
    vel_contr[vel_contr > max_vel] = np.nan
    vel_relax[vel_relax > max_vel] = np.nan

    # calculate mean and std of peak features
    peak_amp_mean, peak_amp_std = np.nanmean(peak_amp), np.nanstd(peak_amp)
    peak_amp_perc_mean, peak_amp_perc_std = np.nanmean(peak_amp_perc), np.nanstd(peak_amp_perc)
    vel_contr_mean, vel_contr_std = np.nanmean(vel_contr), np.nanstd(vel_contr)
    vel_relax_mean, vel_relax_std = np.nanmean(vel_relax), np.nanstd(vel_relax)
    time_contr_10_mean, time_contr_10_std = np.nanmean(time_contr_10), np.nanstd(time_contr_10)
    time_relax_10_mean, time_relax_10_std = np.nanmean(time_relax_10), np.nanstd(time_relax_10)
    time_contr_50_mean, time_contr_50_std = np.nanmean(time_contr_50), np.nanstd(time_contr_50)
    time_relax_50_mean, time_relax_50_std = np.nanmean(time_relax_50), np.nanstd(time_relax_50)

    peak_dict = {'peak_tps': peak_tps, 'peak_amp': peak_amp, 'peak_amp_perc': peak_amp_perc, 'vel_contr': vel_contr,
                 'vel_relax': vel_relax, 'time_contr_10': time_contr_10, 'time_relax_10': time_relax_10,
                 'time_contr_50': time_contr_50, 'time_relax_50': time_relax_50, 'peak_amp_mean': peak_amp_mean,
                 'peak_amp_std': peak_amp_std, 'peak_amp_perc_mean': peak_amp_perc_mean,
                 'peak_amp_perc_std': peak_amp_perc_std, 'vel_contr_mean': vel_contr_mean,
                 'vel_contr_std': vel_contr_std, 'vel_relax_mean': vel_relax_mean, 'vel_relax_std': vel_relax_std,
                 'time_contr_10_mean': time_contr_10_mean, 'time_contr_10_std': time_contr_10_std,
                 'time_relax_10_mean': time_relax_10_mean, 'time_relax_10_std': time_relax_10_std,
                 'time_contr_50_mean': time_contr_50_mean, 'time_contr_50_std': time_contr_50_std,
                 'time_relax_50_mean': time_relax_50_mean, 'time_relax_50_std': time_relax_50_std,
                 'params.min_time': min_time, 'params.max_vel': max_vel}
    return peak_dict


def get_empty_peak_dict():
    peak_dict = {'peak_tps': np.nan, 'peak_amp': np.nan, 'peak_amp_perc': np.nan, 'vel_contr': np.nan,
                 'vel_relax': np.nan, 'time_contr_10': np.nan, 'time_relax_10': np.nan,
                 'time_contr_50': np.nan, 'time_relax_50': np.nan, 'peak_amp_mean': np.nan,
                 'peak_amp_std': np.nan, 'peak_amp_perc_mean': np.nan,
                 'peak_amp_perc_std': np.nan, 'vel_contr_mean': np.nan,
                 'vel_contr_std': np.nan, 'vel_relax_mean': np.nan, 'vel_relax_std': np.nan,
                 'time_contr_10_mean': np.nan, 'time_contr_10_std': np.nan,
                 'time_relax_10_mean': np.nan, 'time_relax_10_std': np.nan,
                 'time_contr_50_mean': np.nan, 'time_contr_50_std': np.nan,
                 'time_relax_50_mean': np.nan, 'time_relax_50_std': np.nan,
                 'params.min_time': np.nan, 'params.max_vel': np.nan}
    return peak_dict

def analyze_delta_tps(peak_tps, plot=False):
    """
    Analysis of time between peaks

    Parameters
    ----------
    peak_tps : ndarray/list
        Time points (in seconds) of peaks
    plot : book
        If True, histogram plot with results is shown.

    Returns
    -------
    mean, std, skew, kurt : tuple
        Mean, standard deviation, skewness and kurtosis of time between peaks.
    """
    delta = np.diff(peak_tps)
    mean, std = np.nanmean(delta), np.nanstd(delta)
    skew, kurt = stats.skew(delta, nan_policy='omit'), stats.kurtosis(delta, nan_policy='omit')

    if plot:
        fig, ax = plt.subplots(figsize=(4, 2))
        ax.hist(delta, range=(0, 6), bins=40, rwidth=0.8, color='grey')
        ax.set_xlabel('Time between peaks [s]')
        ax.set_ylabel('Count')
        text = f'mean={np.round(mean, 2)}\nstd={np.round(std, 2)}\nskew={np.round(skew, 2)}\nkurt.={np.round(kurt, 2)}'
        ax.text(4.3, 2, text)
        plt.show()

    return mean, std, skew, kurt
