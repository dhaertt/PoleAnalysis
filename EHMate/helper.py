import string
import os

import numpy as np


def convert_to_mp4(path, new_filename, old_filename):
    threads = 4
    os.chdir(path)
    try:
        os.system("ffmpeg -i %s -threads %d -f mp4 %s" % (old_filename, threads, new_filename))
        print("ffmpeg completed converting for %s" % old_filename)
        if os.path.exists(new_filename):
            os.remove(old_filename)
    except:
        raise SystemError('Something went wrong')


def idx_to_well_name(idx, cols=6, rows=8):
    coords = np.unravel_index(idx, (cols, rows))
    number = str(coords[0] + 1)
    letter = string.ascii_uppercase[coords[1]]
    return letter + number


def reduce_dataframe(df):
    df_reduced = df.copy()
    for key in df.keys():
        if isinstance(df[key][0], np.ndarray):
            df_reduced.drop(key, axis=1, inplace=True)
    return df_reduced
